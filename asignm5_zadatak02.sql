
-- Zadatatak 2

DELIMITER $$

CREATE PROCEDURE change_manager2(IN emp_id INT, IN dep_id INT)
BEGIN
	
    INSERT INTO MANAGERS (EMPLOYEE_ID, DEP_ID, PROMOTION_DATE)
    VALUES (emp_id, dep_id, CURRENT_DATE());
END $$

DELIMITER ;
