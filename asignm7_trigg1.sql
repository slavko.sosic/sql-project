-- assignment 7: Trigger 1 

create trigger decreassing_salaries_after_dismissal
after update on MANAGERS
for each row
update COMPANY_EMPLOYEES as emp
set SALARY=SALARY - SALARY * 0.1 
where OLD.DISMISSAL_DATE is null and NEW.DISMISSAL_DATE is not null and emp.DEP=OLD.DEP_ID; 

update MANAGERS 
set DISMISSAL_DATE=CURRENT_DATE()
where DEP_ID=5;

select * from COMPANY_EMPLOYEES;
