delimiter //
create function findEmployee() returns varchar(50) deterministic
begin
	return (
				select NAME from COMPANY_EMPLOYEES
				where DATE_FORMAT(CURDATE() + INTERVAL 1 DAY, '%m-%d') = date_format(DATE_OF_EMPLOYMENT, '%m-%d')
		  ); 
end //
delimiter ;


select findEmployee();
