-- assignment 6: Trigger 1 
create trigger after_inserting_manager
after insert on MANAGERS
for each row
update COMPANY_EMPLOYEES as emp
set emp.SALARY=emp.SALARY + emp.SALARY * 0.1
where emp.DEP=NEW.DEP_ID;

insert into MANAGERS 
values (5, 5, '2023-12-25', null);
select * from COMPANY_EMPLOYEES;



